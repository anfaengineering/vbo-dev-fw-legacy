/*
 * Board_Def.h
 *
 *  Created on: Dec 17, 2021
 *      Author: nhq
 */

#ifndef BOARD_DEF_H_
#define BOARD_DEF_H_

#include "stm32g0xx_hal.h"
#include "stm32g0xx_ll_rcc.h"
#include "stm32g0xx_ll_bus.h"
#include "stm32g0xx_ll_system.h"
#include "stm32g0xx_ll_exti.h"
#include "stm32g0xx_ll_cortex.h"
#include "stm32g0xx_ll_utils.h"
#include "stm32g0xx_ll_pwr.h"
#include "stm32g0xx_ll_dma.h"
#include "stm32g0xx_ll_gpio.h"
#include "stm32g0xx_hal_tim.h"
#include "stm32g0xx_ll_usart.h"
#include "stm32g0xx_ll_utils.h"

/**
 * HCLK = 48 Mhz
 *
 * 1. APB1 = HCLK / 1 = 48Mhz
 *    PCLK1 = APB1 = 48Mhz
 *    TIM2, 3, 4, 5, 6, 7, 12, 13, 14 = APB1 = 48Mhz
 *
 **/
#define XTAL_FREQUENCY      ((uint32_t)16000000UL) /* Internal HSI Oscillator frequency 16000000UL */
#define PLL_N               (9u)                   /* PLL N Value */
#define PLL_M               (1u)                   /* PLL M = 1u Value */
#define PLL_P               (2u)                   /* Input PLL Divider PLL_P = 2 for SYSLK = 48MHz, see p.124 FULL manual */
#define PLL_R               (3u)                   /* Input PLL Divider for PLLPCLK clock output = 48MHz */

#define PLL_FREQUENCY       ((uint32_t)((XTAL_FREQUENCY / PLL_M) * PLL_N))
#define SYSCLK_FREQUENCY    ((uint32_t)(PLL_FREQUENCY / PLL_R))
#define HCLK_FREQUENCY      ((uint32_t)(SYSCLK_FREQUENCY))


#define MCO_Pin                 GPIO_PIN_0
#define MCO_GPIO_Port           GPIOF

#define TMS_Pin                 GPIO_PIN_13
#define TMS_GPIO_Port           GPIOA

#define TCK_Pin                 GPIO_PIN_14
#define TCK_GPIO_Port           GPIOA

#define LED4_GPIO_PIN           (GPIO_PIN_5)
#define LED4_GPIO_PORT          (GPIOA)

#define SET_LED4                HAL_GPIO_WritePin(LED4_GPIO_PORT, LED4_GPIO_PIN, GPIO_PIN_SET)
#define CLR_LED4                HAL_GPIO_WritePin(LED4_GPIO_PORT, LED4_GPIO_PIN, GPIO_PIN_RESET)
#define TOGGLE_LED4             HAL_GPIO_TogglePin(LED4_GPIO_PORT, LED4_GPIO_PIN)

#define BUTTON_GPIO_PIN         (GPIO_PIN_13)
#define BUTTON_GPIO_PORT        (GPIOC)

#define GET_BUTTON              HAL_GPIO_ReadPin(BUTTON_GPIO_PORT, BUTTON_GPIO_PIN)


#endif /* BOARD_DEF_H_ */
