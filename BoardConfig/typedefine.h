#ifndef typedefine_H
#define typedefine_H

#include "stm32g0xx_hal.h"
#include <stdio.h>
#include <string.h>

/*****************************************************************************
*       Types
******************************************************************************/

/*****************************************************************************
*       Defines
******************************************************************************/
#define TYPEDEFINE_LITTLE_ENDIAN 1

typedef char s8, *ps8;
typedef unsigned char u8, *pu8;
typedef short s16, *ps16;
typedef unsigned short u16, *pu16;

typedef char iTE_s8, *piTE_s8;
typedef unsigned char iTE_u8, *piTE_u8;
typedef short iTE_s16, *piTE_s16;
typedef unsigned short iTE_u16, *piTE_u16;

typedef unsigned long iTE_u32, *piTE_u32;
typedef long iTE_s32, *piTE_s32;
typedef unsigned long u32, *pu32;
typedef long s32, *ps32;

typedef unsigned char u1;
typedef unsigned char iTE_u1;

typedef unsigned int  bool_t;

#ifndef NULL
    /*SAFETYMCUSW 218 S MR:20.2 <APPROVED> "Custom Type Definition." */
    #define NULL ((void *) 0U)
#endif

#ifndef NULL_PTR
    #define NULL_PTR ((void *)0x0)
#endif

#ifndef TRUE
    #define TRUE 1
#endif

#ifndef FALSE
    #define FALSE 0
#endif


#define BIT(X)  (1 << (X))

#define BIT0    0x00000001
#define BIT1    0x00000002
#define BIT2    0x00000004
#define BIT3    0x00000008
#define BIT4    0x00000010
#define BIT5    0x00000020
#define BIT6    0x00000040
#define BIT7    0x00000080

#define BIT8    0x00000100
#define BIT9    0x00000200
#define BIT10   0x00000400
#define BIT11   0x00000800
#define BIT12   0x00001000
#define BIT13   0x00002000
#define BIT14   0x00004000
#define BIT15   0x00008000

#define BIT16   0x00010000
#define BIT17   0x00020000
#define BIT18   0x00040000
#define BIT19   0x00080000
#define BIT20   0x00100000
#define BIT21   0x00200000
#define BIT22   0x00400000
#define BIT23   0x00800000

#define BIT24   0x01000000
#define BIT25   0x02000000
#define BIT26   0x04000000
#define BIT27   0x08000000
#define BIT28   0x10000000
#define BIT29   0x20000000
#define BIT30   0x40000000
#define BIT31   0x80000000

#define MASK_bit0 (1 << 0)
#define MASK_bit1 (1 << 1)
#define MASK_bit2 (1 << 2)
#define MASK_bit3 (1 << 3)
#define MASK_bit4 (1 << 4)
#define MASK_bit5 (1 << 5)
#define MASK_bit6 (1 << 6)
#define MASK_bit7 (1 << 7)
#define CONCAT(a, b) a ## b
#define MASK(x) CONCAT(MASK_, x)

/* with volatile */
typedef volatile unsigned char uchar_8;
typedef volatile unsigned short uint_16;
typedef volatile unsigned long ulong_32;

/* no volatile */
typedef unsigned char BYTE;
typedef unsigned short WORD;
typedef unsigned long DWORD;

/* For const table at ILM */
typedef unsigned char CBYTE;
typedef unsigned short CWORD;
typedef unsigned long CDWORD;

typedef volatile unsigned char FLAG;

typedef void* HANDLE;
typedef unsigned char BOOL;

typedef signed char SINT8;
typedef signed short SINT16;
typedef signed long SINT32;
typedef signed int SINT;

typedef SINT8* LPSINT8;
typedef SINT16* LPSINT16;
typedef SINT32* LPSINT32;
typedef SINT* LPSINT;

typedef unsigned char UINT8;
typedef unsigned short UINT16;
typedef unsigned long UINT32;
typedef unsigned long long UINT64;
typedef unsigned int UINT;

typedef UINT8* LPUINT8;
typedef UINT16* LPUINT16;
typedef UINT32* LPUINT32;
typedef UINT* LPUINT;

typedef char INT8;
typedef short INT16;
typedef long INT32;
typedef long long INT64;

#ifdef TYPEDEFINE_LITTLE_ENDIAN

#define mBYTE0_OF_WORD(x)      (*((UBYTE *)(&x)+0))
#define mBYTE1_OF_WORD(x)      (*((UBYTE *)(&x)+1))

#define mBYTE0_OF_DWORD(x)     (*((UBYTE *)(&x)+0))
#define mBYTE1_OF_DWORD(x)     (*((UBYTE *)(&x)+1))
#define mBYTE2_OF_DWORD(x)     (*((UBYTE *)(&x)+2))
#define mBYTE3_OF_DWORD(x)     (*((UBYTE *)(&x)+3))

#define mWORD0_OF_DWORD(x)     (*((UWORD *)(&x)+0))
#define mWORD1_OF_DWORD(x)     (*((UWORD *)(&x)+1))

#define mWORD_OF_BYTE(a,b)      ( ( (UWORD) b ) <<  8 | a )

#define mDWORD_OF_BYTE(a,b,c,d) ( ( (UDWORD)d ) << 24 | ( (UDWORD) c ) << 16 | ( (UDWORD) b ) <<  8 | a )

#endif

#ifdef TYPEDFINE_BIG_ENDIAN

#define mBYTE0_OF_WORD(x)      (*((UBYTE *)(&x)+1))
#define mBYTE1_OF_WORD(x)      (*((UBYTE *)(&x)+0))

#define mBYTE0_OF_DWORD(x)     (*((UBYTE *)(&x)+3))
#define mBYTE1_OF_DWORD(x)     (*((UBYTE *)(&x)+2))
#define mBYTE2_OF_DWORD(x)     (*((UBYTE *)(&x)+1))
#define mBYTE3_OF_DWORD(x)     (*((UBYTE *)(&x)+0))

#define mWORD0_OF_DWORD(x)     (*((UWORD *)(&x)+1))
#define mWORD1_OF_DWORD(x)     (*((UWORD *)(&x)+0))


#endif

#define mGET_BIT0(x)            (x &  0x01)
#define mGET_BIT1(x)            (x &  0x02)
#define mGET_BIT2(x)            (x &  0x04)
#define mGET_BIT3(x)            (x &  0x08)
#define mGET_BIT4(x)            (x &  0x10)
#define mGET_BIT5(x)            (x &  0x20)
#define mGET_BIT6(x)            (x &  0x40)
#define mGET_BIT7(x)            (x &  0x80)

#define mSET_BIT0(x)            (x |= 0x01)
#define mSET_BIT1(x)            (x |= 0x02)
#define mSET_BIT2(x)            (x |= 0x04)
#define mSET_BIT3(x)            (x |= 0x08)
#define mSET_BIT4(x)            (x |= 0x10)
#define mSET_BIT5(x)            (x |= 0x20)
#define mSET_BIT6(x)            (x |= 0x40)
#define mSET_BIT7(x)            (x |= 0x80)

#define mCLR_BIT0(x)            (x &= ~0x01)
#define mCLR_BIT1(x)            (x &= ~0x02)
#define mCLR_BIT2(x)            (x &= ~0x04)
#define mCLR_BIT3(x)            (x &= ~0x08)
#define mCLR_BIT4(x)            (x &= ~0x10)
#define mCLR_BIT5(x)            (x &= ~0x20)
#define mCLR_BIT6(x)            (x &= ~0x40)
#define mCLR_BIT7(x)            (x &= ~0x80)

#define mGET_BIT(num,bitloc)    ((num &= (1<<bitloc))>>bitloc)
#define mSET_BIT(num,bitloc)    (num  |= (1<<bitloc))
#define mCLR_BIT(num,bitloc)    (num  &= ~(1<<bitloc))

#define IN_RANGE(x,min,max) ((((x)>=(min))&&((x)<=(max))))

#ifndef NULL
    /** Definition of NULL if not built-in. */
    /*lint -estring(960,NULL) Suppress unavoidable warning about re-use of C90 reserved identifier. */
    #define NULL ((void*)0)
#endif

#define ZERO8               ((UBYTE)  0x00)         /**< '0' vom Typ uint8_t  */
#define ZERO16              ((UWORD) 0x0000)       /**< '0' vom Typ uint16_t */
#define ZERO32              ((UDWORD) 0x00000000)   /**< '0' vom Typ uint32_t */

#ifndef BIT
#define BIT(n)              (1 << (n))
#endif

#endif /* typedefine_H */
