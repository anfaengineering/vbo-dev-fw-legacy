/*
 * I2C_Driver.h
 *
 *  Created on: Dec 17, 2021
 *      Author: nhq
 */

#ifndef I2C_DRIVER_H_
#define I2C_DRIVER_H_

#include "Board_Def.h"
#include "typedefine.h"

extern void I2CDriverInit(void);
extern void I2CTransferTest(void);
extern iTE_u1 i2c_write_byte(iTE_u8 address, iTE_u8 offset, iTE_u8 byteno, iTE_u8 *p_data, iTE_u8 device);
extern iTE_u1 i2c_read_byte(iTE_u8 address, iTE_u8 offset, iTE_u8 byteno, iTE_u8 *p_data, iTE_u8 device);


#endif /* I2C_DRIVER_H_ */
