/*****************************************************************************
 *                                 Includes
 *****************************************************************************/

/*---------------------------- Standard Header ------------------------------*/

/*-------------------------- Specific Source Header -------------------------*/

#include <sys/_types.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "Board_Def.h"
#include "UartDebug_Driver.h"


/*-------------------------------- Constants --------------------------------*/

/*---------------------------------- Macros ---------------------------------*/

/*---------------------------------- Types ----------------------------------*/


/*---------------------------------- Import ---------------------------------*/

/*---------------------------------- Export ---------------------------------*/

/*----------------------------  Global Variables ----------------------------*/

extern void Error_Handler(void);

/*------------------------------Local Variables -----------------------------*/
UART_HandleTypeDef huart2;

static uint8_t uRxIndexCnt;
static uint8_t uDebugRxBuf[SERVICE_RX_MSG_SIZE];  /* Buffer for Service-Console */

/*----------------------- Local Functions prototypes ----------------------*/
/* Callback function to hand-over data to upper layer. */
static UartDebug_ReceiveCallback_t S_fpReceiveCb;

static void UART_CharReception_Callback(void);
static void MX_USART2_UART_Init(void);
static size_t console_write(const char *p_uData, size_t uLength);

static void UART_Error_Callback(void);

uint8_t aTxStartMessage[] = "Hello World\n\r";

/*****************************************************************************
 *                          Global Functions Implementation
 *****************************************************************************/

/*
 * See the serial2.h header file.
 */
void UartDebugInit(void)
{
    MX_USART2_UART_Init();

    S_fpReceiveCb = NULL;
    uRxIndexCnt = 0u;

    /* Enable RXNE and Error interrupts */
    LL_USART_EnableIT_RXNE(USART2);
    LL_USART_EnableIT_ERROR(USART2);
} /* UartDebugInit() */

/*****************************************************************************/
/**
 * \brief   Registration call back function for receiving data via SCI0 RXI0
 *          using interrupt.
 *          Register callback for receiving a message from the service interface.
 *          It will be called in the upper layer.
 *
 * \param  fpCallback   call back function.
 *
 * \return None.
 */
/*****************************************************************************/
void UartDebugRegisterReceiveCallback(UartDebug_ReceiveCallback_t fpCallback)
{
    if ((NULL== S_fpReceiveCb) && (NULL != fpCallback))
    {
        S_fpReceiveCb = fpCallback;
    }
} /* UartDebugRegisterReceiveCallback() */

int sprintf(char *str, const char *fmt, ...)
{
    int m;

    __va_list va;
    va_start(va, fmt);
    m = vsnprintf(str, 0xffffff, fmt, va);
    va_end(va);

    return m;
}

int snprintf(char *str, size_t n, const char *fmt, ...)
{
    int m;

    __va_list va;
    va_start(va, fmt);
    m = vsnprintf(str, n, fmt, va);
    va_end(va);

    return m;
}

int debugPrintf(const char *fmt, ...)
{
    char str[512];
    int m;

    __va_list va;
    va_start(va, fmt);
    m = vsnprintf(str, sizeof(str), fmt, va);
    va_end(va);

    console_write(str, m);

    return m;
}

/**
 * I/O function used by printf()
 *
 * A carriage return will be added before each newline
 *
 * \param uTxData Character byte
 * \return The number of bytes written
 */
int console_write_byte(char uTxData)
{
    LL_USART_TransmitData8(USART2, (uint8_t)uTxData);

    return (uTxData);
} /* console_write_byte */

/**
 * I/O function used by printf()
 *
 * A carriage return will be added before each newline
 *
 * \param buf   Pointer to the buffer
 * \param nbyte Number of bytes in the buffer
 * \return The number of bytes written
 */
static size_t console_write(const char *p_uData, size_t uLength)
{
    /* Send the  Message */
    if (HAL_UART_Transmit(&huart2, (uint8_t*)p_uData, uLength, 1000)!= HAL_OK)
    {
        /* Transfer error in transmission process */
        Error_Handler();
    }

    return (uLength);
} /* console_write() */

/**
  * @brief This function handles USART2 global interrupt / USART2 wake-up interrupt through EXTI line 26.
  */
void USART2_IRQHandler(void)
{
  /* Check RXNE flag value in ISR register */
  if(LL_USART_IsActiveFlag_RXNE(USART2) && LL_USART_IsEnabledIT_RXNE(USART2))
  {
    /* RXNE flag will be cleared by reading of RDR register (done in call) */
    /* Call function in charge of handling Character reception */
    UART_CharReception_Callback();
  }

  if(LL_USART_IsEnabledIT_ERROR(USART2) && LL_USART_IsActiveFlag_NE(USART2))
  {
    /* Call Error function */
    UART_Error_Callback();
  }

  /* USER CODE END USART2_IRQn 0 */
  HAL_UART_IRQHandler(&huart2);
}

/*****************************************************************************
 *                          Local Functions Implementation
 *****************************************************************************/
/**
  * @brief  Rx Transfer completed callback
  * @note   This example shows a simple way to report end of IT Rx transfer, and
  *         you can add your own implementation.
  * @retval None
  */
static void UART_CharReception_Callback(void)
{
  /* Read Received character. RXNE flag is cleared by reading of RDR register */
  uint8_t rxByte = LL_USART_ReceiveData8(USART2);

  uDebugRxBuf[uRxIndexCnt++] = rxByte;     /* Store them in buffer */

  /* Send back data to terminal */
  LL_USART_TransmitData8(USART2, (uint8_t)rxByte);

  if (uRxIndexCnt >=0)
  {
      if (rxByte == 0x0d || rxByte == 0x0a)
      {
          /* Call call back */
          if (S_fpReceiveCb != NULL)
          {
              S_fpReceiveCb((uint8_t *)uDebugRxBuf);
              memset((void *)uDebugRxBuf, 0 , sizeof(uDebugRxBuf));
              uRxIndexCnt = 0;
          }
      }
      else if (rxByte == 0x08 || rxByte == 0x7f)
      {
          /* Check for Backspace */
          LL_USART_TransmitData8(USART2, 32);
          LL_USART_TransmitData8(USART2, '\b');
          uRxIndexCnt--;
      }
  }
}

/**
  * @brief  UART error callbacks
  * @note   This example shows a simple way to report transfer error, and you can
  *         add your own implementation.
  * @retval None
  */
static void UART_Error_Callback(void)
{
   uint32_t isr_reg;

  /* Disable USARTx_IRQn */
  NVIC_DisableIRQ(USART2_IRQn);

  /* Error handling example :
    - Read USART ISR register to identify flag that leads to IT raising
    - Perform corresponding error handling treatment according to flag
  */
  isr_reg = LL_USART_ReadReg(USART2, ISR);
  if (isr_reg & LL_USART_ISR_NE)
  {
    /* Turn LED4 off: Transfer error in reception/transmission process */
      CLR_LED4;
  }
  else
  {
    /* Turn LED4 off: Transfer error in reception/transmission process */
      CLR_LED4;
  }
}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{
  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.Init.ClockPrescaler = UART_PRESCALER_DIV1;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetTxFifoThreshold(&huart2, UART_TXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetRxFifoThreshold(&huart2, UART_RXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_DisableFifoMode(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */
}
