/*****************************************************************************
 *                                 Includes
 *****************************************************************************/

/*---------------------------- Standard Header ------------------------------*/

/*-------------------------- Specific Source Header -------------------------*/

#include "typedefine.h"

#include "Board_Def.h"
#include "I2C_Driver.h"
#include "UartDebug_Driver.h"

extern void Error_Handler(void);


/*-------------------------------- Constants --------------------------------*/
/* Timing register value is computed with the STM32CubeMX Tool,
 * Fast Mode @400kHz with I2CCLK = 64 MHz,
 * rise time = 100ns, fall time = 10ns
 * Timing Value = (uint32_t)0x10707dbc
 */
#define I2C_TIMING               ((uint32_t)(0xB << 28u) | (uint32_t)(0x04 << 20u) | (uint32_t)(0x2 << 16u) | (uint32_t)(0xF << 8u) | (uint32_t)(0x13))//(0x10707dbc)
#define I2C_MASTER_ADDRESS       (0u)
#define I2C_MASTER_ADDRESS       (0u)
#define I2C_SLAVE_ADDRESS        (0xAFu)
#define I2C_TX_MSG_SIZE          (64)
#define I2C_MAX_TIMEOUT          (50u)

/*---------------------------------- Macros ---------------------------------*/

/*---------------------------------- Types ----------------------------------*/


/*---------------------------------- Import ---------------------------------*/

/*---------------------------------- Export ---------------------------------*/

/*----------------------------  Global Variables ----------------------------*/


/*------------------------------Local Variables -----------------------------*/

I2C_HandleTypeDef hi2c1;
DMA_HandleTypeDef hdma_i2c1_tx;
DMA_HandleTypeDef hdma_i2c1_rx;

UINT8  g_RxDMABuffer[256u];
UINT8  g_TxDMABuffer[256u];

/*----------------------- Local Functions prototypes ----------------------*/

static void MX_I2C1_Init(void);
static UINT8 i2c_block_dma_read(UINT8 ucCH, UINT8 ucSlaveID, UINT8 MemAddress, UINT8 ucWCount, UINT8* pOutputBuf, UINT8 ucRCount);
static UINT8 i2c_block_dma_write(UINT8 ucCH,UINT8 ucSlaveID, UINT8 ucAddr, UINT8* pInputBuf, UINT8 ucWCount);

/*****************************************************************************
 *                          Global Functions Implementation
 *****************************************************************************/
void I2CDriverInit(void)
{
    MX_I2C1_Init();
}

iTE_u1 i2c_write_byte(iTE_u8 address, iTE_u8 offset, iTE_u8 byteno, iTE_u8 *p_data, iTE_u8 device)
{
    if( i2c_block_dma_write(device, address, offset, p_data, byteno) )
    {
        debugPrintf("i2c_write_byte fail: device = 0x%02X, addr=0x%02X, offset=0x%02X, num=0x%02X, data=0x%02X \r\n", device, address, offset, byteno, *p_data);
        return 0;
    }
    else
    {
        return 1;
    }
}

iTE_u8 i2c_6805 = 0;
iTE_u1 i2c_read_byte(iTE_u8 address, iTE_u8 offset, iTE_u8 byteno, iTE_u8 *p_data, iTE_u8 device)
{
    if(i2c_block_dma_read(device, address, offset, 1, p_data, byteno))
    {

        debugPrintf("i2c_read_byte fail: device = 0x%02X, addr=0x%02X, offset=0x%02X, num=0x%02X \r\n", device, address, offset, byteno);
        if(i2c_6805==0)
        {
            i2c_6805 = 1;
            debugPrintf("6805 address 0x90 I2C fail, 6805 board unpluged \r\n\r\n");
        }

        return 0;
    }
    else
    {
        return 1;
    }
}



void I2CTransferTest(void)
{
    uint8_t aTxBuffer[] = "Hello World";

#if (iTE6807 == true)

    /* Wait for User push-button press before starting the Communication */
    while (GET_BUTTON != GPIO_PIN_RESET)
    {
    }

    /* Delay to avoid that possible signal rebound is taken as button release */
    LL_mDelay(50);

    /* Wait for User push-button release before starting the Communication */
    while (GET_BUTTON != GPIO_PIN_SET)
    {
    }

    do
    {
        if (HAL_I2C_Master_Transmit_IT(&hi2c1, (uint16_t)I2C_SLAVE_ADDRESS, (uint8_t *)aTxBuffer, sizeof(uint8_t)) != HAL_OK)
        {
            /* Error_Handler() function is called when error occurs. */
            Error_Handler();
        }

        while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY)
        {
        }
    } while (HAL_I2C_GetError(&hi2c1) == HAL_I2C_ERROR_AF);
#endif
}

/**
 * @brief This function handles I2C1 event global interrupt / I2C1 wake-up interrupt through EXTI line 23.
 */
void I2C1_IRQHandler(void)
{
    if (hi2c1.Instance->ISR & (I2C_FLAG_BERR | I2C_FLAG_ARLO | I2C_FLAG_OVR))
    {
        HAL_I2C_ER_IRQHandler(&hi2c1);
    }
    else
    {
        HAL_I2C_EV_IRQHandler(&hi2c1);
    }
}

/**
 * @brief This function handles DMA1 channel 1 interrupt.
 */
void DMA1_Channel1_IRQHandler(void)
{
    HAL_DMA_IRQHandler(&hdma_i2c1_tx);
}

/**
 * @brief This function handles DMA1 channel 2 and channel 3 interrupts.
 */
void DMA1_Channel2_3_IRQHandler(void)
{
    HAL_DMA_IRQHandler(&hdma_i2c1_rx);
}

/*****************************************************************************
 *                          Local Functions Implementation
 *****************************************************************************/
/**
 * @brief I2C1 Initialization Function
 * @param None
 * @retval None
 */
static void MX_I2C1_Init(void)
{
    /* USER CODE END I2C1_Init 1 */
    hi2c1.Instance = I2C1;
    hi2c1.Init.Timing = I2C_TIMING;
    hi2c1.Init.OwnAddress1 = I2C_MASTER_ADDRESS;
    hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
    hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
    hi2c1.Init.OwnAddress2 = I2C_MASTER_ADDRESS;
    hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
    hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
    hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
    if (HAL_I2C_Init(&hi2c1) != HAL_OK)
    {
        debugPrintf("Error I2C Init.\r\n");
    }
    /** Configure Analogue filter */
    if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
    {
        debugPrintf("Error I2C Analogue Filter\r\n");
    }
    /** Configure Digital filter */
    if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
    {
        debugPrintf("Error I2C Digital Filter\r\n");
    }
}

/****************************************************************************************/
/**
 * Function name: i2c_block_dma_read()
 * Description:
 *   i2c block dma read
 * Arguments:
 *   UINT8 ucCH: Channel 0:D, 1:E, 2:F.
 *   UINT8 ucSlaveID: I2C slave address
 *   UINT8* pInputBuf: Write SRAM address
 *   UINT8 ucWCount: Write data count
 *   UINT8* pOutputBuf: Read SRAM address
 *   UINT8 ucRCount: Read data count
 * Return Values:
 *   1: bus busy
 *   0: read action done
 * Note:
 *   none
 */
/****************************************************************************************/
static UINT8 i2c_block_dma_read(UINT8 ucCH, UINT8 ucSlaveID, UINT8 MemAddress, UINT8 ucWCount, UINT8* pOutputBuf, UINT8 ucRCount)
{
    UINT8 uRetVal = 0;
    HAL_StatusTypeDef eGetReadStatus;
    UINT32 uStartTimerMs;
    UINT32 uActualTimerMs;

    /* Start to get the current time counter */
    uStartTimerMs = HAL_GetTick();
    /* Start reading process */
    do
    {
        uActualTimerMs = HAL_GetTick() - uStartTimerMs;
        eGetReadStatus = HAL_I2C_Mem_Read(&hi2c1, (UINT16)ucSlaveID, (UINT16)MemAddress, sizeof(UINT8), (UINT8 *)pOutputBuf, ucRCount, I2C_MAX_TIMEOUT);

        if ((UINT32)I2C_MAX_TIMEOUT < uActualTimerMs)
        {
            /* Timeout */
            uRetVal = 1;
        }
    } while ((eGetReadStatus != HAL_OK) && (uRetVal != 0));

    return (uRetVal);
}

/****************************************************************************************/
/**
 * Function name: i2c_block_dma_write()
 * Description:
 *   i2c block dma write
 * Arguments:
 *   UINT8 ucCH: Channel 0:D, 1:E, 2:F.
 *   UINT8 ucSlaveID: I2C slave address
 *   UINT8* pInputBuf: Write SRAM address
 *   UINT8 ucWCount: Write data count
 * Return Values:
 *   -1: bus busy
 *   0: read action done
 * Note:
 *   none
 */
/****************************************************************************************/
static UINT8 i2c_block_dma_write(UINT8 ucCH,UINT8 ucSlaveID, UINT8 ucAddr, UINT8* pInputBuf, UINT8 ucWCount)
{
    UINT8* pi2c_Buf;
    int i;

    pi2c_Buf = (UINT8*)g_TxDMABuffer;
    pi2c_Buf[0]=ucAddr;

    /* Assign write data */
    for(i = 0; i < ucWCount; i++)
    {
        pi2c_Buf[i] = pInputBuf[i];
    }

    UINT8 uRetVal = 0;
    HAL_StatusTypeDef eGetReadStatus;
    UINT32 uStartTimerMs;
    UINT32 uActualTimerMs;

    /* Start to get the current time counter */
    uStartTimerMs = HAL_GetTick();
    /* Start reading process */
    do
    {
        uActualTimerMs = HAL_GetTick() - uStartTimerMs;
        eGetReadStatus = HAL_I2C_Mem_Write(&hi2c1, ucSlaveID, (uint16_t)ucAddr, sizeof(uint8_t), (uint8_t *)pi2c_Buf, (uint16_t)ucWCount, I2C_MAX_TIMEOUT);

        if ((UINT32)I2C_MAX_TIMEOUT < uActualTimerMs)
        {
            /* Timeout */
            uRetVal = 1;
        }
    } while ((eGetReadStatus != HAL_OK) && (uRetVal != 0));

    return (uRetVal);
}

