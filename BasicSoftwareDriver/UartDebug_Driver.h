/*****************************************************************************/
/**
 *  \brief
 *
 *
 *  \file     BSVC_ServiceBus_Driver.h
 *
 *  \author   Hong Quang Nguyen
 *
 *  \version  See Git.
 *
 *  \date     2020-03-14
 *
 *  \copyright  Copyright (C) 2020 eSystems. All rights reserved.
 */
/*****************************************************************************/
/*****************************************************************************
 * History :
 *              \date        YYYY-MM-DD
 *              \brief       Description of changes
 *              \author      author of changes
 *****************************************************************************/
/**
 * @addtogroup BSVC_ServiceBus_Driver
 * @{
 * @file BSVC_ServiceBus_Driver.c BSVC_ServiceBus_Driver.h
 *
 */
/*****************************************************************************
 *                                 Includes
 *****************************************************************************/

/*---------------------------- Standard Header ------------------------------*/

/*-------------------------- Specific Source Header -------------------------*/

#ifndef SERIAL_COMMS_H
#define SERIAL_COMMS_H

#include "typedefine.h"

/* The system clock divider defining the maximum baud rate supported by the UART. */
#define SERVICE_RX_MSG_SIZE     (64)

/**< Function type for callback function used to forward received bytes */
typedef void (*UartDebug_ReceiveCallback_t) (uint8_t *pCommand);

extern void UartDebugInit(void);

extern void UartDebugRegisterReceiveCallback(UartDebug_ReceiveCallback_t fpCallback);

extern int debugPrintf(const char *fmt, ...);

#endif

