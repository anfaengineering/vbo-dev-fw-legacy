/*****************************************************************************/
/**
 *  \brief    This file publishes the functions for setting the timers
 *            that are used within the HS4-A10 package
 *
 *  \file     BTMR_Timer_Driver.c
 *
 *  \author   Hong Quang Nguyen <hong.nguyen@voith.com>
 *
 *  \version  See Git.
 *
 *  \date     2016-08-15
 *
 *  \copyright  Copyright (C) 2016 VOITH. All rights reserved.
 */
/*****************************************************************************/
/*****************************************************************************
 * History :
 *              \date        YYYY-MM-DD
 *              \brief       Description of changes
 *              \author      author of changes
 *****************************************************************************/
/**
 * @addtogroup BTMR_Timer_Driver
 * @{
 * @file BTMR_Timer_Driver.c BTMR_Timer_Driver.h
 *
 * Implementation of HS4-A10 firmware based on the cortex-m4 STM32F427IGT6
 * platform.
 */

#ifndef HTMR_TIMER_DRIVER_H
#define HTMR_TIMER_DRIVER_H

/*****************************************************************************
 *                                 Includes
 *****************************************************************************/
/*---------------------------- Standard Header ------------------------------*/
#include "Board_Def.h"
#include "typedefine.h"

/*-------------------------- Specific Source Header -------------------------*/

/*****************************************************************************
 *                                  Defines
 *****************************************************************************/


/*-------------------------------- Constants --------------------------------*/

/*---------------------------------- Macros ---------------------------------*/

/*---------------------------------- Types ----------------------------------*/
typedef enum
{
	eSYSTEMTASK0 = 0,
	eSYSTEMTASK1,
	eSYSTEM_TASK_MAX
} BTMR_eSystemTask_t;


/**
 * \brief Function type for callback function used to forward received bytes
 */
typedef void (*BTMR_SystemTaskCb_t)(void);
/*---------------------------------- Import ---------------------------------*/

/*---------------------------------- Export ---------------------------------*/

/*----------------------------  Global Variables ----------------------------*/

/*------------------------------Local Variables -----------------------------*/

/*------------------------- Local Functions prototypes ----------------------*/

/*------------------------ Global Functions prototypes ----------------------*/

/**< Interrupt handler Timer11 with 100us resolution */
extern void TIM3_IRQHandler(void);

/**< Interrupt handler Timer15 with 50000us resolution */
extern void TIM15_IRQHandler(void);

/**< Initialize the timers.*/
extern void BTMR_InitTimers(void);


/**< Register the SystemTask into Timer Interrupts */
extern bool_t BTMR_RegisterSystemTaskCallback(BTMR_eSystemTask_t eSystemTask, BTMR_SystemTaskCb_t fpCallback);

#endif /* HTMR_TIMER_DRIVER_H */
/*****************************************************************************/
/**
 * Close the Doxygen group.
 * ! @}
 */
/*****************************************************************************/
