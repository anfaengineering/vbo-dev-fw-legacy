/*****************************************************************************/
/**
 *  \brief    This file publishes the functions for setting the timers
 *            that are used within the HS4-A10 package
 *
 *  NOTE:     The following tasks are used: SystemTasks0, SystemTasks1
 *  		  and SystemTasks2. For each task, we need one Timer for handling.
 *  		  + SystemTasks0 contains: UART polling, SystemBoosterTimerTask;
 *  		  + SystemTasks1 contains: SystemInputRead, LMS, RAM parameters, CAN;
 *  		  + SystemTasks2 contains: PROFIbus, UART state machine;
 *
 *  \file     BTMR_Timer_Driver.c
 *
 *  \author   Hong Quang Nguyen <hong.nguyen@voith.com>
 *
 *  \version  See Git.
 *
 *  \date     2016-10-15
 *
 *  \copyright  Copyright (C) 2016 VOITH. All rights reserved.
 */
/*****************************************************************************/
/*****************************************************************************
 * History :
 *              \date        YYYY-MM-DD
 *              \brief       Description of changes
 *              \author      author of changes
 *****************************************************************************/
/**
 * @addtogroup BTMR_Timer_Driver
 * @{
 * @file BTMR_Timer_Driver.c BTMR_Timer_Driver.h
 *
 * Implementation of HS4-A10 firmware based on the cortex-m4 STM32F427IGT6
 * platform.
 */

/*****************************************************************************
 *                                 Includes
 *****************************************************************************/
#pragma GCC optimize ("0")

/*---------------------------- Standard Header ------------------------------*/

#include "typedefine.h"
#include "Board_Def.h"
#include "Timer_Driver.h"
#include "UartDebug_Driver.h"

/*-------------------------- Specific Source Header -------------------------*/

/*****************************************************************************
 *                                  Defines
 *****************************************************************************/

/*-------------------------------- Constants --------------------------------*/
#define MAX_NR_SYSTEMTASK_CB    	(3u)

#define PRESCALER_VALUE (uint32_t)((HCLK_FREQUENCY / 10000) - 1)
#define PERIOD_VALUE (10000 - 1)

/*---------------------------------- Macros ---------------------------------*/

/*---------------------------------- Types ----------------------------------*/

/*---------------------------------- Import ---------------------------------*/

/*---------------------------------- Export ---------------------------------*/

/*----------------------------  Global Variables ----------------------------*/

/*------------------------------Local Variables -----------------------------*/

/* SYSTEMTASK0 calls every 100us */
static TIM_HandleTypeDef stTIM3SystemTask0; /* TIM3 handler declaration */

/* SYSTEMTASK1 calls every 50ms */
static TIM_HandleTypeDef stTIM15SystemTask1; /* TIM4 handler declaration */


/* Callback function Array */
static BTMR_SystemTaskCb_t fpSystemTaskCb[eSYSTEM_TASK_MAX];

/*----------------------- Local Functions prototypes ----------------------*/
/* Initialize Timer 3 register */
static void InitTimer3Register(void);

/* Initialize Timer 15 register */
static void InitTimer15Register(void);

/*****************************************************************************
 *                          Global Functions Implementation
 *****************************************************************************/
/*****************************************************************************/
/**
 * \brief 	Interrupt handler TIM3 for SystemTasks0  with 16 bit with
 * 		  	resolution 100us .This function will executed in the VECTOR TABLE
 * 		  	in interruptVectors.c
 *
 * \param  	None.
 *
 * \return	 None.
 */
/*****************************************************************************/
void TIM3_IRQHandler(void)
{
	if(NULL != fpSystemTaskCb[eSYSTEMTASK0])
	{
		/* Execute TASK 0 */
		fpSystemTaskCb[eSYSTEMTASK0]();
	}

	/* Clear the interrupt. */
	HAL_TIM_IRQHandler(&stTIM3SystemTask0);
} /* TIM1_TRG_COM_TIM3_IRQHandler() */

/*****************************************************************************/
/**
 * \brief 	Interrupt handler TIM11 for SystemTasks1 with 16 bit with
 * 		  	resolution 500us .This function will executed in the VECTOR TABLE
 * 		  	in interruptVectors.c
 * 		  	This TIMER13 handle the SystemTasks1 with time resolution 500us = 2Khz.
 *          The APB1 = 84Mhz, thus T13 = APB1 / [(Prescaler + 1) * (Period + 1)]
 *           => 84 Mhz / 4200 / 10 = 2000Hz
 *
 * \param  None.
 *
 * \return None.
 */
/*****************************************************************************/
void TIM15_IRQHandler(void)
{
	if(NULL != fpSystemTaskCb[eSYSTEMTASK1])
	{

		/* Execute TASK 1 */
		fpSystemTaskCb[eSYSTEMTASK1]();
	}

	/* Clear the interrupt. */
	HAL_TIM_IRQHandler(&stTIM15SystemTask1);
} /* TIM8_UP_TIM13_IRQHandler() */

/*****************************************************************************/
/**
 *  \brief	This function configures the operating mode of the timer(s).
 *
 * \param  	None.
 *
 * \return 	None.
 */
/*****************************************************************************/
void BTMR_InitTimers(void)
{
	(void)memset(fpSystemTaskCb, 0u, sizeof(fpSystemTaskCb));

	/* Init. Timer 3 */
	InitTimer3Register();

	/* Init. Timer 15 */
	InitTimer15Register();
} /* HTMR_InitTimers() */

/*****************************************************************************/
/**
 *  \brief	This function registers all the callback for timers
 *
 * \param  	eSystemTask type function task.
 * \param	fpCallback 	callback function.
 *
 * \return 	None.
 */
/*****************************************************************************/
bool_t BTMR_RegisterSystemTaskCallback(BTMR_eSystemTask_t eSystemTask, BTMR_SystemTaskCb_t fpCallback)
{
	bool_t bResult;       /* R�ckgabewert, FALSE bei max. eSystemTask */

	bResult = TRUE;

	switch (eSystemTask)
	{
	case eSYSTEMTASK0:
	{
		fpSystemTaskCb[eSYSTEMTASK0] = fpCallback;

		/* Start Timer 11*/
		HAL_TIM_Base_Start_IT(&stTIM3SystemTask0);
	}

	break;

	case eSYSTEMTASK1:
	{
		fpSystemTaskCb[eSYSTEMTASK1] = fpCallback;

		/* Start Timer 13 */
		HAL_TIM_Base_Start_IT(&stTIM15SystemTask1);
	}

	break;

	case eSYSTEM_TASK_MAX:
	default:
	{
		bResult = FALSE;
		break;
	}
	}

	return (bResult);
} /* BTMR_RegisterSystemTaskCallback() */

/*****************************************************************************
 *                          Local Functions Implementation
 *****************************************************************************/
/*****************************************************************************/
/**
 * \brief   This TIMER3 handle the SystemTasks0 with time resolution 100us = 10Khz.
 *          The APB = 48Mhz, thus T3 = APB / [(Prescaler + 1) * (Period + 1)]
 *           => 48 Mhz / 480 / 10 = 10000Hz
 *
 * \param 	None.
 *
 * \return 	None.
 */
/*****************************************************************************/
static void InitTimer3Register(void)
{
    /* Enable the RCC clock for TIM3 */
    __HAL_RCC_TIM3_CLK_ENABLE();

    TIM_ClockConfigTypeDef sClockSourceConfig = {0};
    TIM_MasterConfigTypeDef sMasterConfig = {0};

    /* USER CODE BEGIN TIM3_Init 1 */

    /* USER CODE END TIM3_Init 1 */
    stTIM3SystemTask0.Instance = TIM3;
    stTIM3SystemTask0.Init.Prescaler = 479;
    stTIM3SystemTask0.Init.CounterMode = TIM_COUNTERMODE_UP;
    stTIM3SystemTask0.Init.Period = 9;
    stTIM3SystemTask0.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    stTIM3SystemTask0.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
    if (HAL_TIM_Base_Init(&stTIM3SystemTask0) != HAL_OK)
    {
        debugPrintf("Error Timer 3 Init.\r\n");
    }
    sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
    if (HAL_TIM_ConfigClockSource(&stTIM3SystemTask0, &sClockSourceConfig) != HAL_OK)
    {
        debugPrintf("Error Timer 3 Clock Source.\r\n");
    }
    sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
    sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
    if (HAL_TIMEx_MasterConfigSynchronization(&stTIM3SystemTask0, &sMasterConfig) != HAL_OK)
    {
        debugPrintf("Error Timer 3 Master Config. Clock Source.\r\n");
    }

    /* TIM3 interrupt Init */
    HAL_NVIC_SetPriority(TIM3_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(TIM3_IRQn);
} /* InitTimer11Register() */

/*****************************************************************************/
/**
 * \brief   This TIMER14 handle the SystemTasks2 with time resolution 50ms = 20Hz
 *          The APB = 48Mhz, thus T15 = APB / [(Prescaler + 1) * (Period + 1)]
 *           => 48 Mhz / 480 / 10 = 20Hz
 *
 * \param 	None.
 *
 * \return 	None.
 */
/*****************************************************************************/
static void InitTimer15Register(void)
{
	/* Enable the RCC clock for TIM3 */
    __HAL_RCC_TIM15_CLK_ENABLE();

    TIM_ClockConfigTypeDef sClockSourceConfig = {0};
    TIM_MasterConfigTypeDef sMasterConfig = {0};

    stTIM15SystemTask1.Instance = TIM15;
    stTIM15SystemTask1.Init.Prescaler = 2399;
    stTIM15SystemTask1.Init.CounterMode = TIM_COUNTERMODE_UP;
    stTIM15SystemTask1.Init.Period = 999;
    stTIM15SystemTask1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    stTIM3SystemTask0.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
    if (HAL_TIM_Base_Init(&stTIM15SystemTask1) != HAL_OK)
    {
        debugPrintf("Error Timer 15 Init.");
    }
    sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
    if (HAL_TIM_ConfigClockSource(&stTIM15SystemTask1, &sClockSourceConfig) != HAL_OK)
    {
        debugPrintf("Error Timer 15 Clock Source.");
    }
    sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
    sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
    if (HAL_TIMEx_MasterConfigSynchronization(&stTIM15SystemTask1, &sMasterConfig) != HAL_OK)
    {
        debugPrintf("Error Timer 15 Master Config. Clock Source.\r\n");
    }

    /* TIM15 interrupt Init */
    HAL_NVIC_SetPriority(TIM15_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(TIM15_IRQn);
} /* InitTimer14Register() */

/*****************************************************************************/
/**
 * Close the Doxygen group.
 * ! @}
 */
/*****************************************************************************/
