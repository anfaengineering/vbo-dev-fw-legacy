/*****************************************************************************
 *                                 Includes
 *****************************************************************************/
/*---------------------------- Standard Header ------------------------------*/
#include <stdio.h>


/*-------------------------- Specific Source Header -------------------------*/
#include "main.h"
#include "I2C_Driver.h"
#include "UartDebug_Driver.h"
#include "trace.h"
#include "iTE6805_DEV_DEFINE.h"
#include "iTE6805_SYS.h"
#include "Timer_Driver.h"

/*****************************************************************************
 *                                  Defines
 *****************************************************************************/
/**  Softpack Version */
#define SOFTPACK_VERSION       "0.2"
#define COMPILER_NAME          "ARM GCC"

#define SHORT_VERSION_STRING    "V145\n    "
#define VERSION_INTERNAL_STRING "iTE6805_IT6350_SAMPLE_1.45_EPLAGPBW"
#define VERSION_EDID_SERIALID   0x145

#define VERID0  '1'
#define VERID1  '4'
#define VERID2  '5'

#define VER_MAJOR   0x01
#define VER_MINOR   0x45

/*---------------------------------- Types ----------------------------------*/
typedef enum
{
    STATE_MAIN_NOT_SET              = 0u,
    STATE_MAIN_STARTUP              = 1u,
    STATE_MAIN_START_SYSTEM         = 4u,
    STATE_MAIN_INIT_IT6807_SYSTEM   = 5u,
    STATE_MAIN_WAIT_ACT             = 6u,
    STATE_MAIN_ERROR                = 7u,

    STATE_MAIN_ENUM_ELEMENT_MAX
} MAIN_eState_t;

typedef struct
{
    MAIN_eState_t eStateCurrent;

    MAIN_eState_t eStateNext;
} MAIN_stStateControl_t;

typedef enum
{
    STAT_MCU_INIT,
    STAT_CHECK_TRAPING,
    STAT_CHECK_DEV_READY,
    STAT_DEV_INIT,
    STAT_IDLE,
} INIT_eiTE6807_t;

/*---------------------------------- Import ---------------------------------*/

/*---------------------------------- Export ---------------------------------*/

/*----------------------------  Global Variables ----------------------------*/
// 6805 global data
_iTE6805_DATA            iTE6805_DATA;
_iTE6805_VTiming         iTE6805_CurVTiming;
_iTE6805_PARSE3D_STR        iTE6805_EDID_Parse3D;

/*------------------------------Local Variables -----------------------------*/

static MAIN_stStateControl_t stMainStateControl;
static INIT_eiTE6807_t eInitStateiTE607;
static uint8_t uTimer100PreScaler = 0;
static bool_t bIsSystemStarted = FALSE;

/*------------------------- Local Functions prototypes ----------------------*/
static void SwitchState(MAIN_stStateControl_t *p_ptMainStateControl);
static void DoStateMainStartup(MAIN_stStateControl_t *ptMAIN_StateControl);
static void DoStateStartSystem(MAIN_stStateControl_t *p_ptMainStateControl);
static void DoStateInit6807System(MAIN_stStateControl_t *p_ptMainStateControl);
static void DoStateMainWaitAction(MAIN_stStateControl_t *p_ptMainStateControl);
static void DoStateMainError(MAIN_stStateControl_t *p_ptMainStateControl);

static void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void Init_It6807(void);

/* Execute in TIM3 System Task 0, executed every 100us HIGHEST - priority 0 */
static void SystemTask0(void);

/* Execute in TIM15 System Task 1, every 50ms, priority 1 */
static void SystemTask1(void);

/*------------------------ Global Functions prototypes ----------------------*/

/*****************************************************************************
 *                          Global Functions Implementation
 *****************************************************************************/
int main(void)
{
    stMainStateControl.eStateCurrent = STATE_MAIN_NOT_SET;
    stMainStateControl.eStateNext    = STATE_MAIN_STARTUP;

    while (TRUE)
    {
        SwitchState(&stMainStateControl);

        switch(stMainStateControl.eStateCurrent)
        {
        case STATE_MAIN_STARTUP:
            DoStateMainStartup(&stMainStateControl);
            break;

        case STATE_MAIN_START_SYSTEM:
            DoStateStartSystem(&stMainStateControl);
            break;

        case STATE_MAIN_INIT_IT6807_SYSTEM:
            DoStateInit6807System(&stMainStateControl);
            break;

        case STATE_MAIN_WAIT_ACT:
            DoStateMainWaitAction(&stMainStateControl);
            break;

        case STATE_MAIN_NOT_SET:
        case STATE_MAIN_ERROR:
        default:
            DoStateMainError(&stMainStateControl);
            break;
        } /* switch */
    }
} /* main() */

void iTE6805_Set_PORT1_HPD(unsigned char temp)
{
    if(temp)
    {
    	debugPrintf("set PORT1 HPD = HIGH \r\n");
        //todo: GPDRA &= 0xFE;
    }
    else
    {
    	debugPrintf("set PORT1 HPD = Low \r\n");
        //todo: GPDRA &= 0x01;
    }
}

/*****************************************************************************
 *                          Local Functions Implementation
 *****************************************************************************/
static void SystemTask0(void)
{
    uTimer100PreScaler++;

    if (uTimer100PreScaler == 10)
    {
        TOGGLE_LED4;
        uTimer100PreScaler = 0;
    }
}

static void SystemTask1(void)
{
    if (TRUE == bIsSystemStarted)
    {
        /* Init IT6807 */
        Init_It6807();
    }
}

static void SwitchState(MAIN_stStateControl_t *p_ptMainStateControl)
{
    p_ptMainStateControl->eStateCurrent = p_ptMainStateControl->eStateNext;
    p_ptMainStateControl->eStateNext = STATE_MAIN_NOT_SET;
} /* SwitchState() */

static void DoStateMainStartup(MAIN_stStateControl_t *p_ptMainStateControl)
{
    /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
    HAL_Init();

    /* Configure the system clock */
    SystemClock_Config();

    /* Normal case */
    p_ptMainStateControl->eStateNext = STATE_MAIN_START_SYSTEM;
} /* DoStateMainStartup */

static void DoStateStartSystem(MAIN_stStateControl_t *p_ptMainStateControl)
{
    bIsSystemStarted = FALSE;

    /* Initialize all configured peripherals */
    MX_GPIO_Init();

    UartDebugInit();

    BTMR_InitTimers();

    I2CDriverInit();

    /* Disable the global interrupt because the SystemTasks will not execute before register them */
    __disable_irq();

    /* Output example information */
    debugPrintf("\r\n");
    debugPrintf("****************************************************\r\n");
    debugPrintf("*** Wallbox 80A Demo                             ***\r\n");
    debugPrintf("*** Software Version %s                         ***\r\n", SOFTPACK_VERSION);
    debugPrintf("*** Compiled: %s %s With %s   ***\r\n", __DATE__, __TIME__, COMPILER_NAME);
    debugPrintf("****************************************************\r\n");
    debugPrintf("\r\n");
    debugPrintf("$ ");

    BTMR_RegisterSystemTaskCallback(eSYSTEMTASK0, &SystemTask0);
    BTMR_RegisterSystemTaskCallback(eSYSTEMTASK1, &SystemTask1);

    /* Enable Global Interrupt to Start the SystemTask*/
    __enable_irq( );

    bIsSystemStarted = TRUE;
    eInitStateiTE607 = STAT_MCU_INIT;

    p_ptMainStateControl->eStateNext = STATE_MAIN_INIT_IT6807_SYSTEM;
} /* DoStateStartSystem() */

static void DoStateInit6807System(MAIN_stStateControl_t *p_ptMainStateControl)
{
    p_ptMainStateControl->eStateNext = STATE_MAIN_WAIT_ACT;
}

static void DoStateMainWaitAction(MAIN_stStateControl_t *p_ptMainStateControl)
{
    //I2CTransferTest();

    /* Normal State, wait for next Action */
    p_ptMainStateControl->eStateNext = STATE_MAIN_WAIT_ACT;
} /* DoStateMainProjectingWaitAction() */

static void DoStateMainError(MAIN_stStateControl_t *p_ptMainStateControl)
{
    /* Folgezustand: Bleibe normalerweise in diesem Zustand */
    p_ptMainStateControl->eStateNext = STATE_MAIN_ERROR;

    /* Stop all */
} /* DoStateMainError() */

static void Init_It6807(void)
{
    switch(eInitStateiTE607)
    {
        case STAT_MCU_INIT:
            eInitStateiTE607 = STAT_CHECK_TRAPING;
            break;

        case STAT_CHECK_TRAPING:
            eInitStateiTE607 = STAT_CHECK_DEV_READY;
            break;

        case STAT_CHECK_DEV_READY:
        	debugPrintf("***********************************\r\n");
        	debugPrintf("version : %s \r\n", VERSION_INTERNAL_STRING);
        	debugPrintf("***********************************\r\n");
            eInitStateiTE607 = STAT_DEV_INIT;
            break;

        case STAT_DEV_INIT:
            eInitStateiTE607 = STAT_IDLE;
            break;

        case STAT_IDLE:
            eInitStateiTE607 = STAT_IDLE;
            iTE6805_FSM();
            break;

        default:
            eInitStateiTE607 = STAT_MCU_INIT;
            break;
    }
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
static void SystemClock_Config(void)
{
    RCC_OscInitTypeDef RCC_OscInitStruct = {0};
    RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

    /** Configure the main internal regulator output voltage
     */
    HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1);
    /** Initializes the RCC Oscillators according to the specified parameters
     * in the RCC_OscInitTypeDef structure.
     */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
    RCC_OscInitStruct.HSIState = RCC_HSI_ON;
    RCC_OscInitStruct.HSIDiv = RCC_HSI_DIV1;
    RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
    RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV1;
    RCC_OscInitStruct.PLL.PLLN = 9;
    RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
    RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV3;
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
    {
        Error_Handler();
    }
    /** Initializes the CPU, AHB and APB buses clocks
     */
    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
            |RCC_CLOCKTYPE_PCLK1;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

    if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
    {
        Error_Handler();
    }

    /* Set systick to 1ms in using frequency set to HCK=48MHz */
    LL_Init1msTick(HCLK_FREQUENCY);

    /* Update CMSIS variable (which can be updated also through SystemCoreClockUpdate function) */
    LL_SetSystemCoreClock(HCLK_FREQUENCY);
}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};

    /* GPIO Ports Clock Enable */
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOF_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();

    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(LED4_GPIO_PORT, LED4_GPIO_PIN, GPIO_PIN_RESET);

    /*Configure GPIO pin : LED_GREEN_Pin */
    GPIO_InitStruct.Pin = LED4_GPIO_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(LED4_GPIO_PORT, &GPIO_InitStruct);

    /*Configure GPIO pin : BUTTON  */
    GPIO_InitStruct.Pin = BUTTON_GPIO_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    HAL_GPIO_Init(BUTTON_GPIO_PORT, &GPIO_InitStruct);
}
